require('dotenv').config();
import { promises } from 'fs';
import { convert } from '../src/convert';

const [filepath] = process.argv.slice(2);

const run = async () => {
  const content = await promises.readFile(filepath, 'utf-8');
  const result = await convert(content);
  const data = JSON.stringify(result, null, 2);
  await promises.writeFile(filepath.replace('.csv', '.json'), data, 'utf-8');
};

run().then(() => process.exit(0)).catch(function (err) {
  console.error(err);
  process.exit(1);
});
