# Hestia Convert Poore&Nemecek2018

[![pipeline status](https://gitlab.com/hestia-earth/hestia-convert-poore-nemecek/badges/develop/pipeline.svg)](https://gitlab.com/hestia-earth/hestia-convert-poore-nemecek/-/commits/master)
[![Coverage Report](https://gitlab.com/hestia-earth/hestia-convert-poore-nemecek/badges/master/coverage.svg)](https://gitlab.com/hestia-earth/hestia-convert-poore-nemecek/commits/master)

Hestia Library to convert Poore&Nemecek format to Hestia format

## Install

```sh
npm install @hestia-earth/poore-nemecek --save
```

## Usage

You can use the bin file directly to convert a CSV file from your command line:
```bash
hestia-convert-poore-nemecek <src file> <dest file>
```
The content will be saved in the dest file. You can them import the file on the Hestia platform!

### Using as a library

You can also integrate the library in your existing NodeJS project:
```typescript
import { readFile } from 'fs';
import { convert } from '@hestia-earth/poore-nemecek';

const csv = await new Promise((resolve, reject) =>
  readFile(join(FIXTURES_FOLDER, 'sample.zip'), (err, data) => err ? reject(err) : resolve(data.toString()))
);
const result = await convert(csv);
```

## Mapping

Please find in the table below the current columns that are handled and the associated Term/value:

| column | converted | Term | Notes |
| ------ | ------ | ------ | ------ |
| Reference used (first author last name, year) | ❌ | | |
| Ingredient/Produce Name | ❌ | | |
| Scientific name of Ingredient/Produce | ❌ | | |
| Country | ✅ | [Site](https://hestia.earth/schema/Site): one of [region](https://hestia.earth/glossary?termType=region) | Use either the `@id` or the exact `name` of the region |
| Geog. Specific Region | ✅ | [Site](https://hestia.earth/schema/Site): one of [region](https://hestia.earth/glossary?termType=region) | Use either the `@id` or the exact `name` of the region |
| Proxy used (if any) | ❌ | | |
| System | ❌ | | |
| ID_FAO | ✅ | [Product](https://hestia.earth/schema/Product) | Use either the `@id` or the exact `name` of the product |
| ID Land use type | ❌ | | |
| Land use type | ❌ | | |
| Crop Name | ❌ | | |
| IPCC Animal | ✅ | [Product](https://hestia.earth/schema/Product) | Use either the `@id` or the exact `name` of the product |
| Seed (kg) / Nursery | ✅ | [Input](https://hestia.earth/schema/Input): [seed](https://hestia.earth/term/seed) | |
| Org N (kg N) | ✅ | [Input](https://hestia.earth/schema/Input): [organicNitrogenFertiliserUnspecifiedKgN](https://hestia.earth/term/organicNitrogenFertiliserUnspecifiedKgN) | Used if all following 4 columns are empty, otherwise will break it down to multiple [Input](https://hestia.earth/schema/Input). |
| Lqd, Slurry & Swg Sludge (%) | ✅ | [Input](https://hestia.earth/schema/Input): [slurryAndSludgeKgN](https://hestia.earth/term/slurryAndSludgeKgN) | value = `"Org N (kg N)" * "Lqd, Slurry & Swg Sludge (%)"` |
| Solid (%) | ✅ | [Input](https://hestia.earth/schema/Input): [manureDryKgN](https://hestia.earth/term/manureDryKgN) | value = `"Org N (kg N)" * "Solid (%)"` |
| Compost (%) | ✅ | [Input](https://hestia.earth/schema/Input): [compostKgN](https://hestia.earth/term/compostKgN) | value = `"Org N (kg N)" * "Compost (%)"` |
| Grn Manr. (%) | ✅ | [Input](https://hestia.earth/schema/Input): [greenManureKgN](https://hestia.earth/term/greenManureKgN) | value = `"Org N (kg N)" * "Grn Manr. (%)"` |
| Org N (kg TAN) | ❌ | | |
| Lqd, Slurry & Swg Sludge (%) | ❌ | | |
| Solid (%) | ❌ | | |
| Compost (%) | ❌ | | | |
| Grn Manr. (%) | ❌ | | |
| Syn N (kg N) | ✅ | [Input](https://hestia.earth/schema/Input): [inorganicNitrogenFertiliserUnspecifiedKgN](https://hestia.earth/term/inorganicNitrogenFertiliserUnspecifiedKgN) | Used if all following 7 columns are empty, otherwise will break it down to multiple [Input](https://hestia.earth/schema/Input). |
| Urea, UAS, (Amm Bicarb) (%) | ✅ | [Input](https://hestia.earth/schema/Input): [ureaKgN](https://hestia.earth/term/ureaKgN) | value = `"Syn N (kg N)" * "Urea, UAS, (Amm Bicarb) (%)"` |
| AS (%) | ✅ | [Input](https://hestia.earth/schema/Input): [ammoniumSulphateKgN](https://hestia.earth/term/ammoniumSulphateKgN) | value = `"Syn N (kg N)" * "AS (%)"` |
| UAN Solu (% | ✅ | [Input](https://hestia.earth/schema/Input): [ureaAmmoniumNitrateKgN](https://hestia.earth/term/ureaAmmoniumNitrateKgN) | value = `"Syn N (kg N)" * "UAN Solu (%)"` |
| AN, ACl, NP, KN, NPK (%) | ✅ | [Input](https://hestia.earth/schema/Input): [inorganicNitrogenFertiliserUnspecifiedKgN](https://hestia.earth/term/inorganicNitrogenFertiliserUnspecifiedKgN) | value = `"Syn N (kg N)" * "AN, ACl, NP, KN, NPK (%)"` |
| CAN (%) | ✅ | [Input](https://hestia.earth/schema/Input): [calciumAmmoniumNitrateKgN](https://hestia.earth/term/calciumAmmoniumNitrateKgN) | value = `"Syn N (kg N)" * "CAN (%)"` |
| AnhA, AquaA (%) | ✅ | [Input](https://hestia.earth/schema/Input): [anhydrousAmmoniaKgN](https://hestia.earth/term/anhydrousAmmoniaKgN) | value = `"Syn N (kg N)" * "AnhA, AquaA  (%)"` |
| AP, DAP, MAP (%) | ✅ | [Input](https://hestia.earth/schema/Input): [diammoniumPhosphateKgN](https://hestia.earth/term/diammoniumPhosphateKgN) | value = `"Syn N (kg N)" * "AP, DAP, MAP (%)"` |
| Excr N (kg N) | ✅ | [Product](https://hestia.earth/schema/Product): [excretaKgN](https://hestia.earth/term/excretaKgN) | |
| Excr TAN (kg TAN) | ❌ | | |
| Org Phos (kg P2O5) | ✅ | [Input](https://hestia.earth/schema/Input): [organicPhosphorusFertiliserUnspecifiedKgP2O5](https://hestia.earth/term/organicPhosphorusFertiliserUnspecifiedKgP2O5) | |
| Syn Phos (kg P2O5) | ✅ | [Input](https://hestia.earth/schema/Input): [inorganicPhosphorusFertiliserUnspecifiedKgP2O5](https://hestia.earth/term/inorganicPhosphorusFertiliserUnspecifiedKgP2O5) | |
| Excr Phos (kg P2O5) | ✅ | [Input](https://hestia.earth/schema/Input): [phosphateContentAsP2O5](https://hestia.earth/term/phosphateContentAsP2O5) | Converted as [Property](https://hestia.earth/schema/Property) of `"Excr N (kg N)"`. Value = `"Excr Phos (kg P2O5)" / "Excr N (kg N)" * 100` |
| Org Pot (kg K2O) | ✅ | [Input](https://hestia.earth/schema/Input): [organicPotassiumFertiliserUnspecifiedKgK2O](https://hestia.earth/term/organicPotassiumFertiliserUnspecifiedKgK2O) | |
| Syn Pot (kg K2O) | ✅ | [Input](https://hestia.earth/schema/Input): [inorganicPotassiumFertiliserUnspecifiedKgK2O](https://hestia.earth/term/inorganicPotassiumFertiliserUnspecifiedKgK2O) | |
| Lime (kg CaCO3) | ✅ | [Input](https://hestia.earth/schema/Input): [lime](https://hestia.earth/term/lime) | |
| Dolomite (kg CaMg(CO3)2) | ✅ | [Input](https://hestia.earth/schema/Input): [dolomiticLime](https://hestia.earth/term/dolomiticLime) | |
| Fuel (kg) for Machinery | ✅ | [Input](https://hestia.earth/schema/Input): [diesel](https://hestia.earth/term/diesel) | |
| Mchn (kg) | ✅ | [Input](https://hestia.earth/schema/Input): [machineryInfrastructureDepreciatedAmountPerCycle](https://hestia.earth/term/machineryInfrastructureDepreciatedAmountPerCycle) | |
| Mchn (hr) | ✅ | [Practice](https://hestia.earth/schema/Practice): [machineryUseOperationUnspecified](https://hestia.earth/term/machineryUseOperationUnspecified) | |
| Plastic (kg) | ✅ | [Input](https://hestia.earth/schema/Input): [plasticTypeUnspecified](https://hestia.earth/term/plasticTypeUnspecified) | |
| Irrigated (Yes/No) | ✅ | [Practice](https://hestia.earth/schema/Practice) | If `Yes`, used with "Irrigation Type (Surface, Sprinkler, Localized)": [Localized](https://hestia.earth/term/irrigatedLocalizedIrrigation); [Surface](https://hestia.earth/term/irrigatedSurfaceIrrigation); [Sprinkler](https://hestia.earth/term/irrigatedSprinklerIrrigation). If `No`, set as [rainfed](https://hestia.earth/term/rainfed)
| Irrigation Type (Surface, Sprinkler, Localized) | ✅ | [Practice](https://hestia.earth/schema/Practice) | See above |
| Fld Appl. Irrig (m3) | ✅ | [Input](https://hestia.earth/schema/Input): [waterSourceUnspecified](https://hestia.earth/term/waterSourceUnspecified) | |
| Energy (Elec, kWh) Irrigation and Other Uses | ✅ | [Input](https://hestia.earth/schema/Input): [electricityGridMarketMix](https://hestia.earth/term/electricityGridMarketMix) with [operation](https://hestia.earth/schema/Input#operation) = [irrigating](https://hestia.earth/term/irrigating) | |
| Energy (Fuel, kWh) Irrigation and Other Uses | ✅ | [Input](https://hestia.earth/schema/Input): [diesel](https://hestia.earth/term/diesel) with [operation](https://hestia.earth/schema/Input#operation) = [irrigating](https://hestia.earth/term/irrigating) | value = `"Energy (Fuel, kWh) Irrigation and Other Uses" * 3.59 / 42.8` |
| Dry/Gr (Elec, kWh) | ✅ | [Input](https://hestia.earth/schema/Input): [electricityGridMarketMix](https://hestia.earth/term/electricityGridMarketMix) with [operation](https://hestia.earth/schema/Input#operation) = [dryingMachineUnspecified](https://hestia.earth/term/dryingMachineUnspecified) | |
| Dry/Gr (Fuel, kWh) | ✅ | [Input](https://hestia.earth/schema/Input): [diesel](https://hestia.earth/term/diesel) with [operation](https://hestia.earth/schema/Input#operation) = [dryingMachineUnspecified](https://hestia.earth/term/dryingMachineUnspecified) | value = `"Dry/Gr (Fuel, kWh)" * 3.59 / 42.8` |
| Pest (kg) | ✅ | [Input](https://hestia.earth/schema/Input): [pesticideUnspecifiedAi](https://hestia.earth/term/pesticideUnspecifiedAi) | Used if all following 3 columns are empty, otherwise will break it down to multiple [Input](https://hestia.earth/schema/Input). |
| Pesticide Type (1) (with next "%" column) | ✅ | [Input](https://hestia.earth/schema/Input) | Use either the `@id` or the exact `name` of the pesticide. Value = `"Pest (kg)" * "%"` |
| Pesticide Type (2) (with next "%" column) | ✅ | [Input](https://hestia.earth/schema/Input) | Use either the `@id` or the exact `name` of the pesticide. Value = `"Pest (kg)" * "%"` |
| Pesticide Type (3) (with next "%" column) | ✅ | [Input](https://hestia.earth/schema/Input) | Use either the `@id` or the exact `name` of the pesticide. Value = `"Pest (kg)" * "%"` |
| Gross Yield (kg or litres of FM, LW or raw milk) | ❌ | | |
| Yield (kg DM) | ✅ | [Property](https://hestia.earth/schema/Property): [dryMatter](https://hestia.earth/term/dryMatter) | value = `"Yield (kg DM)" * 100 / ("Market Yield (kg/l mkt)" or "Yield (kg/l mkt)")` |
| Market Yield (kg/l mkt) | ✅ | [Product](https://hestia.earth/schema/Product) | Set as [primary](https://hestia.earth/schema/Product#primary) product. Can also use `"Yield (kg/l mkt)"` |
| Yield (kg/l mkt) | ✅ | [Product](https://hestia.earth/schema/Product) | Set as [primary](https://hestia.earth/schema/Product#primary) product. Can also use `"Market Yield (kg/l mkt)"` |
| Yield (kg FM, LW or l milk) | ✅ | [Product](https://hestia.earth/schema/Product): [genericCropProduct](https://hestia.earth/term/genericCropProduct) | |
| Econ Alloc (%) | ✅ |[Product economicValueShare](https://hestia.earth/schema/Product#economicValueShare) | Defaults to `100` if not specified |
| Cultiv Dur (yrs) | ✅ | [Practice](https://hestia.earth/schema/Practice): [plantationLifespan](https://hestia.earth/term/plantationLifespan) | value = `"Cultiv Dur (yrs)" * 365.25` |
| Cropping Alloc | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [croppingIntensity](https://hestia.earth/term/croppingIntensity) | value = `"Cropping Alloc" / 100` |
| Fallow Alloc | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [fallowCorrection](https://hestia.earth/term/fallowCorrection) | value = `"Fallow Alloc" / 100` |
| Rainfall | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) | |
| Precip (mm yr-1) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) | |
| Temperature | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [temperatureAnnual](https://hestia.earth/term/temperatureAnnual) | |
| Av Temp (oC) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [temperatureAnnual](https://hestia.earth/term/temperatureAnnual) | |
| Slope | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [slope](https://hestia.earth/term/slope) | |
| Slope (%) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [slope](https://hestia.earth/term/slope) | value = `"Slope (%)" * 100` |
| Slope length | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [slopeLength](https://hestia.earth/term/slopeLength) | |
| Eco-Clim Zone | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [ecoClimateZone](https://hestia.earth/term/ecoClimateZone) | |
| Drainage Class | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [drainageClass](https://hestia.earth/term/drainageClass) | |
| Soil pH (H2O) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [soilPh](https://hestia.earth/term/soilPh) | |
| SOC (% wt, 0-30cm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [organicCarbonPerKgSoil](https://hestia.earth/term/organicCarbonPerKgSoil) | value = `"SOC (% wt, 0-30cm)" * 1000` |
| Soil Phos (g P kg-1, 0-50cm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [phosphorusPerKgSoil](https://hestia.earth/term/phosphorusPerKgSoil) | |
| Soil Nitr (g N kg-1, 0-50cm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [totalNitrogenPerKgSoil](https://hestia.earth/term/totalNitrogenPerKgSoil) | |
| Winter-Type Precip Corr | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [heavyWinterPrecipitation](https://hestia.earth/term/heavyWinterPrecipitation) | |
| Loss Aqu Environ (fraction) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [nutrientLossToAquaticEnvironment](https://hestia.earth/term/nutrientLossToAquaticEnvironment) | value = `"Loss Aqu Environ (fraction)" * 100` |
| Sand (% wt, 0-30cm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [sandContent](https://hestia.earth/term/sandContent) | value = `"Sand (% wt, 0-30cm)" * 100` |
| Clay Content (% wt, 0-30cm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [clayContent](https://hestia.earth/term/clayContent) | value = `"Clay Content (% wt, 0-30cm)" * 100` |
| Erodibility | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [erodibility](https://hestia.earth/term/erodibility) | |
| Histosol | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [histosol](https://hestia.earth/term/histosol) | |
| PET (mm) | ✅ | [Measurement](https://hestia.earth/schema/Measurement): [potentialEvapotranspirationAnnual](https://hestia.earth/term/potentialEvapotranspirationAnnual) | |
| Rem (% AG) | ✅ | [Practice](https://hestia.earth/schema/Practice): [residueRemoved](https://hestia.earth/term/residueRemoved) | |
| Burnt (% ha * CF) | ✅ | [Practice](https://hestia.earth/schema/Practice): [residueBurnt](https://hestia.earth/term/residueBurnt) | |
| Burnt (kg DM) | ❌ | | |
| Residue (kg AG DM) | ❌ | | |
| Residue (kg BG DM) | ❌ | | |
| Crop Res (kg N) | ❌ | | |
| Management | ✅ | [Practice](https://hestia.earth/schema/Practice): one of [tillage](https://hestia.earth/glossary?termType=tillage) | Use by `@id` |
| Root Depth (Max, meters) | ❌ | | |
| Site Type | ✅ | [Site siteType](https://hestia.earth/schema/Site#siteType) | |
| End Date | ✅ | [Cycle endDate](https://hestia.earth/schema/Cycle#endDate) | |
| Urea (CO2) | ✅ | [Emission](https://hestia.earth/schema/Emission): [co2ToAirUreaHydrolysis](https://hestia.earth/term/co2ToAirUreaHydrolysis) | value = `"Urea (CO2)" / 1000 * "Yield (kg/l mkt)"` |
| Lime (CO2) | ✅ | [Emission](https://hestia.earth/schema/Emission): [co2ToAirLimeHydrolysis](https://hestia.earth/term/co2ToAirLimeHydrolysis) | value = `"Lime (CO2)" / 1000 * "Yield (kg/l mkt)"` |

**Note**: all column indicating `%` (e.g. `Solid (%)`) must be written as `81` for `81%`.

### Completeness

The [completeness](https://hestia.earth/schema/Completeness) fields are set by default depending on the values of one or multiple columns. Here's a table showing which columns are used for each completeness.

Note: the [documentation of the schema](https://hestia.earth/schema/Completeness) states in which cases the completeness must be set to `true`, and otherwise set to `false`. If the converted value is incorrect based on the documentation, please [raise an issue](https://gitlab.com/hestia-earth/hestia-convert-poore-nemecek/-/issues/new) providing an example CSV and expected output.

| Completeness | Column(s) used | Set to `True` | Set to `False` |
| ------ | ------ | ------ | ------ |
| [animalFeed](https://hestia.earth/schema/Completeness#animalFeed) | N/A | Always | N/A |
| [cropResidue](https://hestia.earth/schema/Completeness#cropResidue) | `Rem (% AG)`, `Burnt (% ha * CF)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [electricityFuel](https://hestia.earth/schema/Completeness#electricityFuel) | `Fuel (kg) for Machinery`, `Energy (Fuel, kWh) Irrigation and Other Uses`, `Dry/Gr (Fuel, kWh)` or `Energy (Elec, kWh) Irrigation and Other Uses`, `Dry/Gr (Elec, kWh)` | One of the `Fuel` columns has a value **or** one of the `Elec` columns has a value | None of the `Fuel` columns have a value **or** none of the `Elec` columns have a value |
| [excreta](https://hestia.earth/schema/Completeness#excreta) | `Excr N (kg N)`, `Excr Phos (kg P2O5)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [fertiliser](https://hestia.earth/schema/Completeness#fertiliser) | `Org Phos (kg P2O5)`, `Org N (kg N)`, `Syn Phos (kg P2O5)`, `Syn N (kg N)`, `Org Pot (kg K2O)`, `Syn Pot (kg K2O)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [liveAnimals](https://hestia.earth/schema/Completeness#liveAnimals) | N/A | N/A | Always |
| [material](https://hestia.earth/schema/Completeness#material) | `Mchn (kg)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [operations](https://hestia.earth/schema/Completeness#operations) | `Mchn (kg)`, `Mchn (hr)`, `Dry/Gr (Elec, kWh)`, `Dry/Gr (Fuel, kWh)`, `Energy (Elec, kWh) Irrigation and Other Uses`, `Energy (Fuel, kWh) Irrigation and Other Uses` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [other](https://hestia.earth/schema/Completeness#other) | `Seed (kg) / Nursery` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [pesticidesAntibiotics](https://hestia.earth/schema/Completeness#pesticidesAntibiotics) | `Pest (kg)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [products](https://hestia.earth/schema/Completeness#products) | `Yield (kg FM, LW or l milk)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [soilAmendments](https://hestia.earth/schema/Completeness#soilAmendments) | `Lime (kg CaCO3)`, `Dolomite (kg CaMg(CO3)2)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
| [transport](https://hestia.earth/schema/Completeness#transport) | N/A | N/A | Always
| [waste](https://hestia.earth/schema/Completeness#waste) | N/A | N/A | Always |
| [water](https://hestia.earth/schema/Completeness#water) | `Fld Appl Irrig (m3)` | All of the columns are missing, contain `-` or have a value | Any of the columns is empty |
