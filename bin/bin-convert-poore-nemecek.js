#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var index = require("../dist/index");

var start = new Date();
var args = process.argv.slice(2);
var src = args[0];
if (!src) {
  throw new Error('No source file given. Usage: hestia-convert-poore-nemecek <src> <dest>');
}
var dest = args[1];
if (!dest) {
  throw new Error('No dest file given. Usage: hestia-convert-poore-nemecek <src> <dest>');
}

function writeToFile(data) {
  fs.writeFileSync(dest, JSON.stringify(data, null, 2), 'utf-8');
  console.log('Done in ' + (new Date().getTime() - start.getTime()) + 'ms');
  process.exit(0);
}

index.convert(fs.readFileSync(src)).then(writeToFile).catch(function (err) {
  console.error(err);
  process.exit(1);
});
