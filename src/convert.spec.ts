import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { of } from 'rxjs';

import * as terms from './terms';
import { ValueType, getTerm, getProperty, convertDiesel, getTermValueConverted } from './convert';
import * as specs from './convert';

let stubs: sinon.SinonStub[] = [];

describe('convert', () => {
  beforeEach(() => {
    stubs = [];
    stubs.push(sinon.stub(terms, 'preloadMappingTerms').returns(of({})));
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('getTerm', () => {
    beforeEach(() => {
      stubs.push(sinon.stub(terms, 'termData').callsFake(id => Promise.resolve({ '@type': 'Term', '@id': id })));
    });

    describe('mapping found', () => {
      const id = 'Yield (kg DM)';

      it('should return the term data', async () => {
        expect(await getTerm(id)).to.deep.equal({
          '@type': 'Term',
          '@id': 'dryMatter'
        });
      });
    });

    describe('missing mapping', () => {
      const id = 'random-id';

      it('should throw an error', async () => {
        try {
          await getTerm(id);
          expect(true).to.equal(false);
        } catch (err) {
          expect(err.message).to.equal('Missing mapping for random-id');
        }
      });
    });
  });

  describe('getProperty', () => {
    const item = { prop: '10' };

    beforeEach(() => {
      stubs.push(sinon.stub(specs, 'getTerm').returns(Promise.resolve('id')));
    });

    describe('valid property', () => {
      it('should return a Property', async () => {
        const { value } = await getProperty(item, 'prop', ValueType.float);
        expect(value).to.deep.equal(10);
      });
    });

    describe('invalid property', () => {
      it('should return null', async () => {
        expect(await getProperty(item, 'invalid', ValueType.float)).to.equal(null);
      });
    });
  });

  describe('convertDiesel', () => {
    it('should convert from kWh to kg', () => {
      expect(convertDiesel(10)).to.equal(0.838785046728972);
    });
  });

  describe('getTermValueConverted', () => {
    const propertyId = 'dryMatter';
    const defaultProperties = [
      {
        term: {
          '@id': propertyId
        },
        value: 50
      }
    ];
    const yieldKgDm = 50;

    beforeEach(() => {
      stubs.push(sinon.stub(terms, 'termData').returns(Promise.resolve({ defaultProperties })));
    });

    it('should convert back to market yield', async () => {
      expect(await getTermValueConverted('id', propertyId, yieldKgDm)).to.equal(100);
    });
  });
});
