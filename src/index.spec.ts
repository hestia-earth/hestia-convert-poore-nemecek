import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { readFile } from 'fs';
import { join } from 'path';

import { FIXTURES_FOLDER, readFileAsJSON } from '../test/utils';

import { convert } from './index';
import * as terms from './terms';

let stubs: sinon.SinonStub[] = [];

const testSample = (sample: string, forUpload = false) => {
  it('should convert to Hestia', async () => {
    const filepath = join(FIXTURES_FOLDER, `${sample}.csv`);
    const csv = await new Promise<string>((resolve, reject) =>
      readFile(filepath, (err, data) => (err ? reject(err) : resolve(data.toString())))
    );
    const result = await convert(csv, forUpload);
    // console.log(JSON.stringify(result, null, 2));
    expect(result).to.deep.equal(readFileAsJSON(join('hestia', `${sample}.json`)));
  });
};

const termsData = {
  'Oil palm fruit': {
    defaultProperties: [
      {
        term: { '@id': 'dryMatter' },
        value: 53
      }
    ]
  },
  'Rice, paddy': {
    defaultProperties: [
      {
        term: { '@id': 'dryMatter' },
        value: 88
      }
    ]
  },
  genericCropProduct: {
    '@type': 'Term',
    '@id': 'genericCropProduct',
    termType: 'crop'
  },
  dryingMachineUnspecified: {
    '@type': 'Term',
    '@id': 'dryingMachineUnspecified',
    termType: 'operation'
  },
  irrigating: {
    '@type': 'Term',
    '@id': 'irrigating',
    termType: 'operation'
  },
  irrigatedLocalizedIrrigation: {
    '@type': 'Term',
    '@id': 'irrigatedLocalizedIrrigation',
    termType: 'waterRegime'
  },
  irrigatedSurfaceIrrigation: {
    '@type': 'Term',
    '@id': 'irrigatedSurfaceIrrigation',
    termType: 'waterRegime'
  },
  rainfed: {
    '@type': 'Term',
    '@id': 'rainfed',
    termType: 'waterRegime'
  },
  noTillage: {
    '@type': 'Term',
    '@id': 'noTillage',
    termType: 'tillage'
  },
  fullTillage: {
    '@type': 'Term',
    '@id': 'fullTillage',
    termType: 'tillage'
  },
  residueRemoved: {
    '@type': 'Term',
    '@id': 'residueRemoved',
    termType: 'cropResidueManagement'
  },
  residueBurnt: {
    '@type': 'Term',
    '@id': 'residueBurnt',
    termType: 'cropResidueManagement'
  },
  residueLeftOnField: {
    '@type': 'Term',
    '@id': 'residueLeftOnField',
    termType: 'cropResidueManagement'
  }
};

describe('integration', () => {
  beforeEach(() => {
    stubs = [];

    stubs.push(
      sinon.stub(terms, 'bestMatch').callsFake(name => Promise.resolve({ '@type': 'Term', '@id': name, name }))
    );

    stubs.push(
      sinon.stub(terms, 'termData').callsFake(id => {
        const data = termsData[id] || { '@type': 'Term', '@id': id };
        return Promise.resolve(data);
      })
    );
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('convert', () => {
    describe('sample 1', () => {
      testSample('sample1');
    });

    describe('sample 2', () => {
      testSample('sample2');
    });

    describe('sample 3', () => {
      testSample('sample3');
    });

    describe('sample 4', () => {
      testSample('sample4', true);
    });

    describe('sample 5', () => {
      testSample('sample5', true);
    });

    describe('sample 6', () => {
      testSample('sample6', true);
    });
  });
});
