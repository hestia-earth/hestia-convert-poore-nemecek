import axios from 'axios';
const pick = require('lodash.pick');
const cache = require('memory-cache');
import { from } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';
import { NodeType, TermTermType } from '@hestia-earth/schema';

import * as termsMapping from './mappings/terms.json';

declare let window: any;

const defaultBaseUrl = () => {
  try {
    // will not be set in browser mode
    return process.env.API_URL || 'https://api.hestia.earth';
  } catch (err) {
    return 'https://api.hestia.earth';
  }
};

const getBaseUrl = () => {
  try {
    if ((window?.location?.host ?? '').includes('www-staging')) {
      return 'https://api-staging.hestia.earth';
    }
    return defaultBaseUrl();
  } catch (_err) {
    return defaultBaseUrl();
  }
};

const fields = ['@type', '@id', 'name', 'termType', 'units', 'defaultProperties'];

export const getMapping = (value: string) => (termsMapping as any)[value];

const setCache = (key: string, value: any) => cache.put(key, value);

export const cacheSearch = (query: any) => {
  const cacheKey = JSON.stringify(query);
  const cacheEntry = cache.get(cacheKey);
  return (
    cacheEntry ||
    (async () => {
      const results = await axios.post(`${getBaseUrl()}/search`, {
        limit: 1,
        fields: ['@type', '@id', 'name', 'termType', 'units'],
        query
      });
      setCache(cacheKey, results);
      return results;
    })()
  );
};

export const bestMatch = async (name: string, must = [], must_not = []) => {
  const query = {
    bool: {
      must: [{ match: { '@type': NodeType.Term } }, ...must].filter(Boolean),
      must_not,
      should: [
        { match: { '@id.keyword': { query: name, boost: 100300 } } },
        { match: { 'name.keyword': { query: name, boost: 100200 } } },
        { match: { nameNormalized: { query: name, boost: 100100 } } },
        { match: { name: { query: name, boost: 1000 } } }, // search exact match should come first
        { match: { name: { query: `${name} seed`, boost: 10, fuzziness: 'AUTO' } } }, // improves matching for seeds
        { match: { name: { query: name, boost: 1, fuzziness: 'AUTO' } } },
        { match: { 'termType.keyword': { query: TermTermType.crop, boost: 10000 } } }
      ],
      minimum_should_match: 1
    }
  };
  const {
    data: {
      results: [result]
    }
  } = name ? await cacheSearch(query) : { data: { results: [] } };
  return result
    ? pick(result, fields)
    : name
    ? (() => {
        throw new Error(`Term matching "${name}" not found.`);
      })()
    : null;
};

export const termData = (id: string) => {
  const cacheEntry = cache.get(id);
  return (
    cacheEntry ||
    (async () => {
      const data = pick((await axios.get(`${getBaseUrl()}/terms/${id}`)).data, fields);
      setCache(id, data);
      return data;
    })()
  );
};

export const preloadMappingTerms = () =>
  from(Object.values(termsMapping)).pipe(
    filter(Boolean),
    mergeMap((id: string) => from(termData(id)))
  );
