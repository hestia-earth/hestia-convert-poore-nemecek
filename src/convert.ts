import {
  Completeness,
  CycleDefaultMethodClassification,
  CycleFunctionalUnit,
  EmissionMethodTier,
  MeasurementMethodClassification,
  NodeType,
  Product,
  SchemaType,
  SiteSiteType,
  Term,
  TermTermType
} from '@hestia-earth/schema';
import * as csvtojson from 'csvtojson';
import { from } from 'rxjs';
import { map, mergeMap, toArray, tap } from 'rxjs/operators';
import { isEmpty, reduceUndefinedValues } from '@hestia-earth/utils';

import { bestMatch, termData, getMapping, preloadMappingTerms } from './terms';
import * as termsIds from './mappings/term-ids.json';

const getId = (termId: string) => {
  const id = termsIds[termId]?.['@id'];
  if (!id) {
    throw new Error(`Missing id in "term-ids.json" for ${termId}`);
  }
  return id;
};

export enum ValueType {
  boolean = 'boolean',
  float = 'float'
}

const secondaryProductColumn = 'Yield (kg FM, LW or l milk)';
const fertColumn = 'Org N (kg TAN)';
const fertColumns = ['Lqd, Slurry & Swg Sludge (%)', 'Solid (%)', 'Compost (%)', 'Grn Manr (%)'];
const newFertColumns = [fertColumn, ...fertColumns.map(col => `"Tan ${col}"`)];
const pestColumns = ['Pest (1)', 'Pest (2)', 'Pest (3)'];
const newPestColumns = ['Pest (1)', 'Pest (1) %', 'Pest (2)', 'Pest (2) %', 'Pest (3)', 'Pest (3) %'];

// remove . that breaks the CSV parsing
const cleanCsv = (csv: string) =>
  csv
    .replace(/([a-z])(\.)/g, '$1')
    .replace(/[\s]+\n\(kg/g, ' (kg')
    .replace(/\n\(kg/g, ' (kg')
    .replace(`"${fertColumn}"`, fertColumn)
    .replace([fertColumn, ...fertColumns].join(','), newFertColumns.join(','))
    .replace(
      [fertColumn, ...fertColumns.map(c => (c.includes(',') ? `"${c}"` : c))].join(','),
      newFertColumns.join(',')
    )
    .replace([fertColumn, ...fertColumns].join(';'), newFertColumns.join(';'))
    .replace('Pesticide Type (1)', 'Pest (1)')
    .replace('Pesticide Type (2)', 'Pest (2)')
    .replace('Pesticide Type (3)', 'Pest (3)')
    .replace(pestColumns.map(v => `${v},%`).join(','), newPestColumns.join(','))
    .replace(pestColumns.map(v => `${v};%`).join(';'), newPestColumns.join(';'))
    // characterization for emissions
    .replace('Ind MM (N2O),,Units,Characterization', 'Ind MM (N2O),,Emission Units,Emission Characterization');

const formatId = (id: string) => id.replace(/[\s,\(\)\/\\]/g, '').toLowerCase();

const isCellEmpty = (value: string) => !value || value === '-' || value === 'Incl';

const nonEmptyRow = (data: any) => Object.values(data).some((val: string) => !isCellEmpty(val));

const nonEmptyValue = (value: any) => (isCellEmpty(value) ? '' : value);

const filteredPromises = async (promises: Promise<any>[]) =>
  (await Promise.all(promises)).filter(value => !isEmpty(value));

const parseContent = async (content: string): Promise<any[]> =>
  await csvtojson({ delimiter: 'auto' }).fromString(cleanCsv(content));

const findKey = (item: any, text: string) => Object.keys(item).find(key => key.startsWith(text));

const findValue = (item: any, text: string) => (findKey(item, text) ? nonEmptyValue(item[findKey(item, text)]) : '');

const termId = (term: any) => (term ? term['@id'] : null);

export const getTerm = async (value: string) => {
  const id = getMapping(value);
  if (!id) {
    throw new Error(`Missing mapping for ${value}`);
  }
  return await termData(id);
};

const findRegion = async (name: string, country: Term) => {
  try {
    return await bestMatch(
      name,
      [{ match: { 'termType.keyword': TermTermType.region } }, { match: { gadmCountry: country.name } }],
      [{ match: { gadmLevel: 0 } }]
    );
  } catch (err) {
    return null;
  }
};

const convertValue: {
  [type in ValueType]: (value: string) => any;
} = {
  boolean: (value: string): boolean => (value || '').toLowerCase() === 'yes',
  float: (value: string): number => +(value || '').replace('%', '').replace(',', '')
};

export const convertDiesel = (kWh: number) => (kWh * 3.59) / 42.8;

export const cycleId = (product: Product, endDate: string, site?: any) =>
  formatId([termId(product?.term), site?.region?.name, site?.country?.name, endDate].filter(Boolean).join('-'));

const isPartial = (item: any, fields: string[]) => fields.some(field => item[field] !== '' && item[field] !== '-');

const isComplete = (item: any, fields: string[]) => fields.every(field => item[field] !== '');

const getCompleteness = (item: any) =>
  ({
    type: SchemaType.Completeness,
    animalFeed: true,
    cropResidue: isComplete(item, ['Rem (% AG)', 'Burnt (% ha * CF)']),
    electricityFuel:
      isPartial(item, [
        'Fuel (kg) for Machinery',
        'Energy (Fuel, kWh) Irrigation and Other Uses',
        'Dry/Gr (Fuel, kWh)'
      ]) && isPartial(item, ['Energy (Elec, kWh) Irrigation and Other Uses', 'Dry/Gr (Elec, kWh)']),
    excreta: isComplete(item, ['Excr N (kg N)', 'Excr Phos (kg P2O5)']),
    fertiliser: isComplete(item, [
      'Org Phos (kg P2O5)',
      'Org N (kg N)',
      'Syn Phos (kg P2O5)',
      'Syn N (kg N)',
      'Org Pot (kg K2O)',
      'Syn Pot (kg K2O)'
    ]),
    liveAnimals: false,
    material: isComplete(item, ['Mchn (kg)']),
    operations: isComplete(item, [
      'Mchn (kg)',
      'Mchn (hr)',
      'Dry/Gr (Elec, kWh)',
      'Dry/Gr (Fuel, kWh)',
      'Energy (Elec, kWh) Irrigation and Other Uses',
      'Energy (Fuel, kWh) Irrigation and Other Uses'
    ]),
    other: isComplete(item, ['Seed (kg) / Nursery']),
    pesticidesAntibiotics: isComplete(item, ['Pest (kg)']),
    products: isComplete(item, ['Market Yield (kg/l mkt)']),
    soilAmendments: isComplete(item, ['Lime (kg CaCO3)', 'Dolomite (kg CaMg(CO3)2)']),
    transport: false,
    waste: false,
    water: isComplete(item, ['Fld Appl Irrig (m3)'])
  }) as Partial<Completeness>;

const getRegions = (item: any) =>
  findValue(item, 'Geog Specific')
    .split(',')
    .map(v => v.trim())
    .filter(nonEmptyValue);

const getSites = async (item: any): Promise<any[]> => {
  const country = await bestMatch(item.Country, [
    { match: { 'termType.keyword': TermTermType.region } },
    { match: { gadmLevel: 0 } }
  ]);
  const siteType = findValue(item, 'Site Type').toLowerCase() || SiteSiteType.cropland;
  const siteData = {
    type: NodeType.Site,
    siteType,
    country
  };
  const regions = getRegions(item);
  const sites = (
    await Promise.all(
      regions.map(async region => ({
        ...siteData,
        region: await findRegion(region, country)
      }))
    )
  ).filter((v: any) => !!v.region);
  return sites.length ? sites : [siteData];
};

export const getProperty = async (item: any, key: string, valueType: ValueType, convertValueFunc = (v: any) => v) => {
  const value = convertValue[valueType](item[key]);
  return value
    ? {
        type: SchemaType.Property,
        term: await getTerm(key),
        value: convertValueFunc(value)
      }
    : null;
};

const getEconomicValueShare = (item: any) => convertValue.float(findValue(item, 'Econ Alloc')) || 100;

export const getTermValueConverted = async (id: string, propertyId: string, value: number) => {
  const { defaultProperties } = await termData(id);
  const property = (defaultProperties || []).find(({ term }) => term['@id'] === propertyId);
  return property ? (value * 100) / property.value : null;
};

const productConverted = (product: Product) =>
  product?.value?.length && product.value[0] === 0
    ? {
        ...product,
        economicValueShare: 0,
        revenue: 0,
        currency: 'USD'
      }
    : product;

const productName = (item: any) => findValue(item, 'IPCC Animal') || findValue(item, 'ID_FAO');

const productValue = (item: any) =>
  convertValue.float(findValue(item, 'Market Yield') || findValue(item, 'Yield (kg/l mkt)'));

const getDryMatter = (item: any) => {
  const marketYield = productValue(item);
  return marketYield
    ? getProperty(item, 'Yield (kg DM)', ValueType.float, (v: number) => (v * 100) / marketYield)
    : null;
};

const getPrimaryProduct = async (item: any) => {
  const term = await bestMatch(productName(item));
  const dryMatter = await getDryMatter(item);
  const value = productValue(item);
  return {
    type: SchemaType.Product,
    term,
    economicValueShare: getEconomicValueShare(item),
    properties: [dryMatter].filter(Boolean),
    value: isEmpty(value)
      ? await filteredPromises([
          findValue(item, 'Yield (kg DM)')
            ? getTermValueConverted(
                term['@id'],
                getId('dryMatter'),
                convertValue.float(findValue(item, 'Yield (kg DM)'))
              )
            : null
        ])
      : [value],
    primary: true
  } as Product;
};

const getSecondaryProduct = async (item: any) =>
  isCellEmpty(item[secondaryProductColumn])
    ? null
    : ({
        type: SchemaType.Product,
        term: await termData(getId('genericCropProduct')),
        value: [convertValue.float(item[secondaryProductColumn])]
      } as Product);

const getExcreta = async (item: any) => {
  const term = await getTerm('Excr N (kg N)');
  const value = convertValue.float(item['Excr N (kg N)']);
  return value
    ? {
        type: SchemaType.Product,
        term,
        value: [value],
        properties: [await getProperty(item, 'Excr Phos (kg P2O5)', ValueType.float, v => (v / value) * 100)].filter(
          Boolean
        )
      }
    : null;
};

const getBlankNode = async (item: any, key: string, type = SchemaType.Input, convertValueFunc = (v: any) => v) =>
  isCellEmpty(item[key])
    ? null
    : {
        type,
        term: await getTerm(key),
        value: [convertValueFunc(convertValue.float(item[key]))],
        ...(type === SchemaType.Measurement
          ? {
              methodClassification: MeasurementMethodClassification['geospatial dataset']
            }
          : {})
      };

const extendBlankNode = (node: Promise<any>, data: any) => node.then(v => (v ? { ...v, ...data } : null));

const getGroupedInput = async (item: any, key: string, subkeys: string[]) => {
  const value = convertValue.float(item[key]);
  const input = value
    ? {
        type: SchemaType.Input,
        term: await getTerm(key),
        value: [value]
      }
    : null;
  const inputs = value
    ? await filteredPromises(
        subkeys.map(async subkey => {
          const subvalue = findValue(item, subkey);
          const percent = convertValue.float(subvalue);
          return isCellEmpty(subvalue)
            ? null
            : {
                type: SchemaType.Input,
                term: await getTerm(subkey),
                value: [value * (percent < 1 ? percent : percent / 100)]
              };
        })
      )
    : [];
  return inputs.length ? inputs : input ? [input] : [];
};

const getInputWithOperation = async (item: any, key: string, operation: string, convertValueFunc?) => {
  const input = await getBlankNode(item, key, SchemaType.Input, convertValueFunc);
  return input
    ? {
        ...input,
        operation: await termData(operation)
      }
    : null;
};

const findPesticide = (name: string) => bestMatch(name, [{ match: { 'termType.keyword': TermTermType.pesticideAI } }]);

const getPesticides = async (item: any) => {
  const key = 'Pest (kg)';
  const total = convertValue.float(findValue(item, key));
  const pesticides = await filteredPromises(
    [1, 2, 3].map(async index => {
      const name = nonEmptyValue(item[`Pest (${index})`]);
      const value = nonEmptyValue(item[`Pest (${index}) %`]);
      return name && value
        ? {
            type: SchemaType.Input,
            term: await findPesticide(name),
            value: [(total * convertValue.float(value)) / (value.includes('%') ? 100 : 1)]
          }
        : null;
    })
  );
  return pesticides.length
    ? pesticides
    : [
        {
          type: SchemaType.Input,
          term: await getTerm(key),
          value: [total]
        }
      ];
};

const getIrrigated = async (item: any) => {
  const value = convertValue.boolean(findValue(item, 'Irrigated'));
  const type = findValue(item, 'Irrigation Type');
  const id =
    {
      Localized: getId('irrigatedLocalizedIrrigation'),
      Surface: getId('irrigatedSurfaceIrrigation'),
      Sprinkler: getId('irrigatedSprinklerIrrigation')
    }[type] || getId('irrigated');
  return {
    type: SchemaType.Practice,
    term: await termData(value ? id : getId('rainfed'))
  };
};

const getTillage = async (item: any) => {
  const id = getMapping(item.Management);
  return id
    ? {
        type: SchemaType.Practice,
        term: await termData(id),
        value: [100]
      }
    : null;
};

const residuePractice = async (id: string, value: string) =>
  value === ''
    ? null
    : {
        type: SchemaType.Practice,
        term: await termData(id),
        value: [convertValue.float(value)]
      };

const getResidue = (item: any) => {
  const residueRemoved = findValue(item, 'Rem (%');
  const residueBurnt = findValue(item, 'Burnt (%');
  const residueLeftOnField =
    residueRemoved === '' && residueBurnt === ''
      ? ''
      : `${100 - convertValue.float(residueBurnt) - convertValue.float(residueRemoved)}`;
  return [
    residuePractice(getId('residueRemoved'), residueRemoved),
    residuePractice(getId('residueBurnt'), residueBurnt),
    residuePractice(getId('residueLeftOnField'), residueLeftOnField)
  ].filter(Boolean);
};

const emissionMethod = async (item: any) => {
  const methodName = findValue(item, 'Emission Characterization');
  return methodName
    ? await bestMatch(methodName, [
        {
          bool: {
            should: [
              { match: { 'termType.keyword': TermTermType.methodEmissionResourceUse } },
              { match: { 'termType.keyword': TermTermType.model } }
            ],
            minimum_should_match: 1
          }
        }
      ])
    : null;
};

const getEmission = (methodModel: any) => (blankNode: any) =>
  methodModel
    ? {
        ...blankNode,
        methodModel,
        methodTier: EmissionMethodTier['tier 1']
      }
    : null;

const convertItem = (forUpload: boolean) => async (item: any, index: number) => {
  const id = item.ID || `${index + 1}`;
  const product = productConverted(await getPrimaryProduct(item));
  const endDate = findValue(item, 'End Date') || '2000';
  const products = [product, productConverted(await getSecondaryProduct(item)), await getExcreta(item)].filter(Boolean);
  const inputs = [
    ...(await filteredPromises([
      getBlankNode(item, 'Seed (kg) / Nursery'),
      getBlankNode(item, 'Mchn (kg)'),
      getBlankNode(item, 'Org Phos (kg P2O5)'),
      getBlankNode(item, 'Lime (kg CaCO3)'),
      getBlankNode(item, 'Dolomite (kg CaMg(CO3)2)'),
      getBlankNode(item, 'Syn Phos (kg P2O5)'),
      getBlankNode(item, 'Org Pot (kg K2O)'),
      getBlankNode(item, 'Syn Pot (kg K2O)'),
      getBlankNode(item, 'Fuel (kg) for Machinery'),
      getBlankNode(item, 'Plastic (kg)'),
      getBlankNode(item, 'Fld Appl Irrig (m3)'),
      getInputWithOperation(item, 'Energy (Elec, kWh) Irrigation and Other Uses', getId('irrigating')),
      getInputWithOperation(item, 'Energy (Fuel, kWh) Irrigation and Other Uses', getId('irrigating'), convertDiesel),
      getInputWithOperation(item, 'Dry/Gr (Elec, kWh)', getId('dryingMachineUnspecified')),
      getInputWithOperation(item, 'Dry/Gr (Fuel, kWh)', getId('dryingMachineUnspecified'), convertDiesel)
    ])),
    ...(await getGroupedInput(item, 'Org N (kg N)', ['Lqd, Slurry & Swg Sludge', 'Solid', 'Compost', 'Grn Manr'])),
    ...(await getGroupedInput(item, 'Syn N (kg N)', [
      'Urea, UAS, (Amm Bicarb)',
      'AS',
      'UAN Solu',
      'AN, ACl, NP, KN, NPK',
      'CAN',
      'AnhA, AquaA',
      'AP, DAP, MAP'
    ])),
    ...(await getPesticides(item))
  ].filter(Boolean);
  const method = await emissionMethod(item);
  const emissions = (
    await filteredPromises([
      getBlankNode(item, 'Urea (CO2)', SchemaType.Emission, v => (v / 1000) * product.value[0]),
      getBlankNode(item, 'Lime (CO2)', SchemaType.Emission, v => (v / 1000) * product.value[0])
    ])
  )
    .map(getEmission(method))
    .filter(Boolean);
  const measurements = await filteredPromises([
    getBlankNode(item, 'Rainfall', SchemaType.Measurement),
    getBlankNode(item, 'Precip (mm yr-1)', SchemaType.Measurement),
    getBlankNode(item, 'Temperature', SchemaType.Measurement),
    getBlankNode(item, 'Av Temp (oC)', SchemaType.Measurement),
    getBlankNode(item, 'Slope', SchemaType.Measurement),
    getBlankNode(item, 'Slope (%)', SchemaType.Measurement, v => v * 100),
    getBlankNode(item, 'Slope length', SchemaType.Measurement),
    getBlankNode(item, 'Slope Length', SchemaType.Measurement),
    getBlankNode(item, 'Eco-Climate Zone', SchemaType.Measurement),
    getBlankNode(item, 'Eco-Clim Zone', SchemaType.Measurement),
    getBlankNode(item, 'Drainage Class', SchemaType.Measurement),
    getBlankNode(item, 'Soil pH', SchemaType.Measurement),
    extendBlankNode(getBlankNode(item, 'Soil pH (H2O)', SchemaType.Measurement), { depthUpper: 0, depthLower: 30 }),
    getBlankNode(item, 'Organic carbon (per kg soil)', SchemaType.Measurement),
    extendBlankNode(
      getBlankNode(item, 'SOC (% wt, 0-30cm)', SchemaType.Measurement, v => v * 1000),
      { depthUpper: 0, depthLower: 30 }
    ),
    getBlankNode(item, 'Phosphorus (per kg soil)', SchemaType.Measurement),
    extendBlankNode(getBlankNode(item, 'Soil Phos (g P kg-1, 0-50cm)', SchemaType.Measurement), {
      depthUpper: 0,
      depthLower: 50
    }),
    getBlankNode(item, 'Total nitrogen (per kg soil)', SchemaType.Measurement),
    extendBlankNode(getBlankNode(item, 'Soil Nitr (g N kg-1, 0-50cm)', SchemaType.Measurement), {
      depthUpper: 0,
      depthLower: 50
    }),
    getBlankNode(item, 'Heavy winter precipitation', SchemaType.Measurement),
    getBlankNode(item, 'Winter-Type Precip Corr', SchemaType.Measurement),
    getBlankNode(item, 'Fallow correction', SchemaType.Measurement),
    getBlankNode(item, 'Fallow Alloc', SchemaType.Measurement, v => v / 100),
    getBlankNode(item, 'Cropping intensity', SchemaType.Measurement),
    getBlankNode(item, 'Cropping Alloc', SchemaType.Measurement, v => v / 100),
    getBlankNode(item, 'Nutrient loss to aquatic environment', SchemaType.Measurement),
    getBlankNode(item, 'Loss Aqu Environ (fraction)', SchemaType.Measurement, v => v * 100),
    getBlankNode(item, 'Histosol', SchemaType.Measurement),
    getBlankNode(item, 'Erodibility', SchemaType.Measurement),
    getBlankNode(item, 'Silt content', SchemaType.Measurement),
    getBlankNode(item, 'Sand content', SchemaType.Measurement),
    extendBlankNode(
      getBlankNode(item, 'Sand (% wt, 0-30cm)', SchemaType.Measurement, v => v * 100),
      { depthUpper: 0, depthLower: 30 }
    ),
    getBlankNode(item, 'Clay content', SchemaType.Measurement),
    extendBlankNode(
      getBlankNode(item, 'Clay Content (% wt, 0-30cm)', SchemaType.Measurement, v => v * 100),
      { depthUpper: 0, depthLower: 30 }
    ),
    getBlankNode(item, 'PET (mm)', SchemaType.Measurement)
  ]);
  const cycle = {
    type: NodeType.Cycle,
    completeness: getCompleteness(item),
    defaultSource: {
      type: SchemaType.Source,
      id
    },
    dataPrivate: false,
    ...(item['Start Date'] ? { startDate: item['Start Date'] } : {}),
    endDate,
    cycleDuration: 365,
    defaultMethodClassification: CycleDefaultMethodClassification.modelled,
    defaultMethodClassificationDescription: 'default',
    functionalUnit: CycleFunctionalUnit['1 ha'],
    products,
    inputs,
    practices: await filteredPromises([
      getIrrigated(item),
      getTillage(item),
      ...getResidue(item),
      getBlankNode(item, 'Mchn (hr)', SchemaType.Practice),
      getBlankNode(item, 'Cultiv Dur (yrs)', SchemaType.Practice, v => v * 365.25)
    ]),
    ...(emissions.length ? { emissions } : {})
  };
  const sites = await getSites(item);
  return sites
    .map((site, sIndex) => {
      const siteId = sites.length > 1 ? `${id}-${sIndex + 1}` : id;
      const siteData = reduceUndefinedValues({
        ...site,
        id: siteId,
        measurements
      });
      return [
        {
          ...cycle,
          id: forUpload ? id : cycleId(product, endDate, site),
          site: forUpload
            ? {
                type: SchemaType.Site,
                id: siteId
              }
            : siteData
        },
        forUpload
          ? {
              ...siteData,
              defaultSource: {
                type: SchemaType.Source,
                id: siteId
              },
              dataPrivate: false
            }
          : null
      ];
    })
    .flat()
    .filter(Boolean);
};

export const convert = (content: string, forUpload = false) =>
  from(parseContent(content))
    .pipe(
      map(data => data.filter(nonEmptyRow)),
      tap(preloadMappingTerms),
      mergeMap(data => data),
      mergeMap((data, index) => from(convertItem(forUpload)(data, index)), 2),
      toArray(),
      map(nodes => ({ nodes: nodes.flat().sort((a: any, b: any) => (a.id ?? '').localeCompare(b.id)) }))
    )
    .toPromise();
