# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.6.2](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.6.1...v0.6.2) (2023-08-15)


### Bug Fixes

* **mappings:** replace `orchardDuration` with `plantationLifespan` ([3af6b21](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/3af6b21955c9552c8afd5036cca7f3f763c08517))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.6.0...v0.6.1) (2023-07-31)


### Bug Fixes

* **convert:** migrate `grainDryingMachineUnspecified` to `dryingMachineUnspecified` ([9ca883f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9ca883f0c5c5b85413073a8cf595bdff0b6f43c9))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.5.1...v0.6.0) (2023-07-31)


### ⚠ BREAKING CHANGES

* **package:** Requires schema version `22`.

### Bug Fixes

* **mappings:** update term for "Fallow correction" ([837cd51](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/837cd51a010efddbef047fb426a184af318726bd))


* **package:** use schema version `22` ([4f49007](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/4f4900738b6be49a6c1fde7d1598e4d982324e18))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.5.0...v0.5.1) (2023-06-15)


### Bug Fixes

* **mappings:** replace `electricityMarketMix` with `electricityGridMarketMix` ([7e3026c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7e3026ccad6eee4dff4138a03337efc6302369b2))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.4...v0.5.0) (2023-04-03)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `18`

* **package:** update schema to `18` ([89bd46c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/89bd46c1d6a44ac73ddab7cb9aa922d79a44cda9))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.3...v0.4.4) (2023-03-23)


### Bug Fixes

* **convert:** remove `cycle` prefix from auto-generated id ([70117eb](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/70117ebcc61e3cba62fa07d3d0ced16564f297c2))
* **convert:** use `Excr Phos (kg P2O5)` for `excreta` completeness not `fertiliser` ([15e1415](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/15e14154b26425d88fe97b2705939dba83ae39c6))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.2...v0.4.3) (2023-03-20)


### Bug Fixes

* **convert:** replace `grainDryingHours` with `grainDryingMachineUnspecified` ([5bddd43](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/5bddd43bff42c9fae62e1e6f1b4954354ac020c6))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.1...v0.4.2) (2023-03-15)


### Bug Fixes

* **convert:** set `completeness.products` based on `Market Yield (kg/l mkt)` ([78d606e](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/78d606e531edad643f84e53e81a44ba8cf1572e9))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.4.0...v0.4.1) (2023-03-15)

## [0.4.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.3.1...v0.4.0) (2023-03-03)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `16`

* **package:** update schema to `16` ([af0c6cb](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/af0c6cbf039e546690ad42acc37853028d570c25))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.3.0...v0.3.1) (2022-12-19)


### Bug Fixes

* **mapping:** replace `fallowCorrection` with `longFallowRatio` ([b39a53f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/b39a53fa8f5d242fa2982d97087ff6001ab704f6))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.2.0...v0.3.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **convert:** min schema version is `14`

### Features

* **convert:** add required fields from version 14 ([7ce0aff](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7ce0aff6ffa007139e5e5c43a7a00e51eae6b7f0))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.1.0...v0.2.0) (2022-10-10)


### ⚠ BREAKING CHANGES

* **convert:** min schema version is `12.0.0`

### Bug Fixes

* **convert:** replace `fertilizer` with `fertiliser` ([9ff25ba](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9ff25ba787d01c377598127ace50bc0cfe2fbe0d))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.36...v0.1.0) (2022-09-09)


### ⚠ BREAKING CHANGES

* **package:** min schema supported version is `11.0.0`

* **package:** update schema to `11.0.0` ([9c08823](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9c088237e3d4a769ac663238f772ec97db36eb8d))

### [0.0.36](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.35...v0.0.36) (2022-07-18)


### Bug Fixes

* **mappings:** rename `phosphorusPerKgSoil` with `totalPhosphorusPerKgSoil` ([33143ef](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/33143ef282baeeb77405c5be8980e48801111ec5))

### [0.0.35](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.34...v0.0.35) (2022-03-25)


### Features

* **convert:** handle product value is `0` ([a96704b](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/a96704bfdf215201d9bbd2a095fcbda7afd8d15a))

### [0.0.34](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.33...v0.0.34) (2022-03-22)


### Features

* **convert:** skip empty rows when converting ([995899f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/995899faadbefdecaaabbb52635bdafaa94e2f22))

### [0.0.33](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.32...v0.0.33) (2022-03-22)


### Features

* **convert:** handle completeness for `products` and `cropResidue` ([914ae58](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/914ae589f769621ff035a8dd7f87d56110d0a4e6))

### [0.0.32](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.31...v0.0.32) (2022-03-16)


### Bug Fixes

* **mappings:** convert `AP, DAP, MAP` to `diammoniumPhosphateKgN` ([909bb63](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/909bb63302c86ec35522bdc159b57d3c265a7cbf))

### [0.0.31](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.30...v0.0.31) (2022-03-15)


### Bug Fixes

* **convert:** set `cycleDuration` to `365` ([ddb1b23](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ddb1b23238f96f593498bcdaba94ef12098b4f4b))

### [0.0.30](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.29...v0.0.30) (2022-03-14)


### Bug Fixes

* **mappings:** replace `npkBlendKgN` with `inorganicNitrogenFertilizerUnspecifiedKgN` ([e877cd5](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/e877cd5810db17589794eeb48518f31b6673ac4a))

### [0.0.29](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.28...v0.0.29) (2022-02-28)


### Features

* **convert:** hamdle `Fallow Alloc` and `Cropping Alloc` columns ([ce57634](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ce5763463b5972d7c653b4c811041ca6851348d7))


### Bug Fixes

* **convert:** parse dryMatter from `Market Yield` ([cdee957](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/cdee9572a2fc8d4fae17a01f0b8acf1c7292827f))

### [0.0.28](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.27...v0.0.28) (2022-02-19)


### Bug Fixes

* **convert:** handle lower case boolean value ([bc23b58](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/bc23b58e14c42f9669dbc6433f357aa910d0a3ca))

### [0.0.27](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.26...v0.0.27) (2022-02-18)


### Bug Fixes

* **convert:** include full data for added `Term` ([9eece4d](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9eece4d58b21939e17ce64e79cccb4fefb66c618))

### [0.0.26](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.25...v0.0.26) (2022-02-15)


### Bug Fixes

* **terms:** fallback search hestia.earth ([35a1599](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/35a159967735d770afc0f4736a7cb4206dd08ec9))

### [0.0.25](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.24...v0.0.25) (2022-02-15)


### Features

* **convert:** handle depth upper/lower for `Soil pH` ([e1bce4e](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/e1bce4e6bfaadc0a62f400326cc3a6ca410bf4e6))


### Bug Fixes

* **convert:** fix percent conversion in grouped inputs ([c6f000c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/c6f000ce514fcf45b203a7f2c474e486b8ec186c))

### [0.0.24](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.23...v0.0.24) (2022-02-14)


### Features

* **convert:** handle original P&N measurements and convert to Hestia units ([7f43f44](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7f43f4447f81ddcec70f51cd57f2038a0967bc42))


### Bug Fixes

* **convert:** remove internal `name` field ([3f9db8c](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/3f9db8cd226a03394880946d4ff38a6d77c00404))

### [0.0.23](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.22...v0.0.23) (2022-02-11)


### Bug Fixes

* **convert:** unnest nodes for upload ([9f9d2b8](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9f9d2b88a3b534fac8339fe3454517aea4bcb39d))

### [0.0.22](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.21...v0.0.22) (2022-02-11)


### Bug Fixes

* **convert:** convert `orchardDuration` to days ([46c15e5](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/46c15e5578838e9f9f24f2f7b294797e420af4e7))

### [0.0.21](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.20...v0.0.21) (2022-02-10)


### Features

* **convert:** handle `Cultiv Dur (yrs)` column ([466e5da](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/466e5daf1b9a735c9649b563dc6f5c3890cb7191))
* **terms:** handle fetch from staging ([2ee580d](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/2ee580d951ef86c8d4306643351fd4a11033f5b5))

### [0.0.20](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.19...v0.0.20) (2021-10-20)


### Features

* **mappings:** add more measurements ([a4c8709](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/a4c870952dc5de2113a0f40965c0569fdd32ebbc))

### [0.0.19](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.18...v0.0.19) (2021-10-08)

### [0.0.18](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.17...v0.0.18) (2021-10-08)


### Features

* **convert:** handle grouped Input values as `0` ([e2cdd0f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/e2cdd0fb24931f79f931cb3357003c956467a7fe))

### [0.0.17](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.16...v0.0.17) (2021-10-07)


### Features

* **convert:** handle `0` values ([ae7b7d6](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ae7b7d6d6ee6ba1cba72bb6254276dda0b3b8768))
* **terms:** allow search by hestia exact `[@id](https://gitlab.com/id)` ([2f7a19f](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/2f7a19f2266125608e8b5103bd8eb4d9c2c62181))

### [0.0.16](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.15...v0.0.16) (2021-10-06)


### Bug Fixes

* **convert:** handle emission method as `model` ([58028e0](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/58028e0c001e795a102e8d889e585f969de381c6))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.14...v0.0.15) (2021-10-06)


### Features

* **convert:** handle site measurements ([5f0c171](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/5f0c171b3fe97b9a0840b9c1448f05d025cd68dd))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.13...v0.0.14) (2021-10-01)


### Bug Fixes

* **convert:** handle no emissions method ([7efc4c8](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/7efc4c8f5b860192226e81fcc3ae8b24f1b3488f))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.12...v0.0.13) (2021-10-01)


### Features

* **convert:** handle `co2ToAirUearHydrolysis` and `co2ToAirLimeHydrolysis` emissions ([b195c18](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/b195c1858627467267bb9ebb2e6c29c736306cb0))
* **convert:** handle irrigation type ([dd53214](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/dd532147956df1fc3e3942ae22507b947c625a9f))
* **convert:** handle pesticide types ([72ef346](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/72ef3466f1e4e4e50cfa24f1735fb03cff4ed84a))
* **convert:** handle residue practices ([88588f4](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/88588f4ed55b82dc73ea661384467e0f17b385d2))
* **convert:** handle secondary product ([57a6ffe](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/57a6ffe4316ccad4ed78da0b22700b896baf8826))


### Bug Fixes

* **convert:** do not parse bibliographic information ([689e121](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/689e12190c0071d8839fe38dec2674113f36c7a1))
* **convert:** use marketable yield when provided ([6c76336](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/6c763364b275bb66ab7481822e2f72c0f6e30fbd))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.11...v0.0.12) (2021-09-23)


### Features

* **convert:** handle completeness for `soilAmendments` ([4e3810a](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/4e3810a666af4273205b1abac239942a36d03ef6))
* **convert:** handle excreta and animal products ([9708600](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/97086004fff788a761e4cb660dfa8d95af987fb5))
* **convert:** handle startDate/endDate ([e309db3](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/e309db39438cfcbdd87699853227cc0f50402517))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.10...v0.0.11) (2021-09-22)


### Features

* **convert:** detect partial completeness ([2c8db48](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/2c8db48732f6f6c6ad8172b8fa930b943d143eb1))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.9...v0.0.10) (2021-09-22)


### Bug Fixes

* **convert:** set other completeness to false if no seed ([99e2df0](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/99e2df0f1ad5e2711256e0b6e1a814e93d01ca70))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.8...v0.0.9) (2021-09-21)


### Bug Fixes

* **convert:** fix replace new lines in column headers ([48926c1](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/48926c1b92866accedd94dd25fb24997d96a962f))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.7...v0.0.8) (2021-09-21)


### Features

* **convert:** handle grouped inputs with only global term specified ([b44a76a](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/b44a76a8f7de92d7b0ed8676622cecd454f5eaf4))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.6...v0.0.7) (2021-09-20)


### Bug Fixes

* **convert:** handle new lines in headers ([9d483e2](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/9d483e2cd702bf507842a672b726cb5496e9f55b))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.5...v0.0.6) (2021-09-03)


### Bug Fixes

* **convert:** fix auto-generated id remove invalid chars ([8deed4b](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/8deed4ba6f930bd68a45bd6420bbe3c4e5fa3a75))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.4...v0.0.5) (2021-09-03)


### Features

* **convert:** find regions if possible - default to country ([23739ff](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/23739ffa8fed6fabe7af2675d3614456f1f467bb))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.3...v0.0.4) (2021-09-03)


### Features

* **convert:** handle yield conversion from Dry Matter ([aba5438](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/aba5438c99bef417dc8f59a8ed3117474f02a890))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.2...v0.0.3) (2021-08-23)


### Bug Fixes

* **convert:** add missing `primary` on product ([ded93a7](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/ded93a7c1f2d4b9a1133c2367a5fcf09183af687))
* **convert:** set `functionalUnit` to `1 ha` by default ([878a18a](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/878a18ad01bd6d431c90ea4951de9dadeb45d6e2))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-convert-olca/compare/v0.0.1...v0.0.2) (2021-08-20)


### Features

* **convert:** fetch full term data instead of just `[@id](https://gitlab.com/id)` ([3790480](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/3790480ae71f19ba49b4482ea0d18c5e59843657))

### 0.0.1 (2021-08-20)


### Features

* add function to convert CSV as string ([aa64814](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/aa64814c4c56c07bc4fb5a3761db94d47ff4e6f0))
* **convert:** search country/product match in Hestia's glossary ([18d7f08](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/18d7f08f70bddfefa4034ef14070b2a63cff1be8))


### Bug Fixes

* **convert:** handle diesel as `kWh` ([59482df](https://gitlab.com/hestia-earth/hestia-convert-olca/commit/59482df78bfd2707e298c2167bdc3f45b4bb1b71))
